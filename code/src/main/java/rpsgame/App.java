package rpsgame;

import backend.RpsGame;
import java.util.Scanner;

/**
 * Console application for rock paper scissors game
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //TODO
        RpsGame game = new RpsGame();
        Scanner scan = new Scanner(System.in);

        boolean playAgain = true;

        while(playAgain){
            System.out.println("Enter you move: rock, paper, scissor");
            String playerChoice = scan.nextLine();
            game.playRound(playerChoice);
            System.out.println("Wins: " + game.getWins());
            System.out.println("Losses: " + game.getLosses());
            System.out.println("Ties: " + game.getTies());

            System.out.println("\nWould you like to play again? (y/n)");
            String answer = scan.nextLine();
            if(answer.equals("n")){
                playAgain = false;
            }
        }
    }
}
