package backend;

import java.util.Random;

public class RpsGame {
    //TODO

    private int wins;
    private int ties;
    private int losses;
    private Random random;


    public RpsGame(){
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.random = new Random();
    }

    public int getWins(){
        return this.wins;
    }

    public int getTies(){
        return this.ties;
    }

    public int getLosses(){
        return this.losses;
    }

    public String playRound(String playerChoice){
        int computerChoice = random.nextInt(3);
        String computerMove = convert(computerChoice);

        String winningMessage = determineWinner(playerChoice, computerMove);

        if(winningMessage.equals("Player Wins!")){
            System.out.println("Computer plays " + computerMove + " and the computer lost");
            this.wins++;
        }
        else if(winningMessage.equals("Computer Wins!")){
            System.out.println("Computer plays " + computerMove + " and the computer won");            
            this.losses++;
        }
        else{
            System.out.println("Computer plays " + computerMove + " and it is a tie");
            this.ties++;
        }
        return "Computer plays " + computerMove + " and " + winningMessage;
    }

    private String convert(int num){
        switch (num){
            case 0:
                return "rock";
            case 1:
                return "scissors";
            case 2:
                return "paper";
            default: 
                return "";
        }
    }

    private String determineWinner(String playerChoice, String computerChoice){
        if(playerChoice.equals(computerChoice)){
            return "Tie!";
        }
        else if((playerChoice.equals("rock") && computerChoice.equals("scissors")) || (playerChoice.equals("scissors") && computerChoice.equals("paper")) || (playerChoice.equals("paper") && computerChoice.equals("rock"))){
            return "Player Wins!";
        }
        else{
            return "Computer Wins!";
        }
    }
}
